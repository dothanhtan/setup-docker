<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Docker Tutorial</title>
</head>
<body>
    <div class="container">
        <h1 class="text-center mt-3">Hello World</h1>
        <h2 class="text-center">Welcome to Docker Tutorial</h2>
        <p class="text-center"><?php echo 'We are running PHP, version: ' . phpversion(); ?></p>
        <?  
            $database ="mydb";  
            $user = "root";  
            $password = "secret";  
            $host = "mysql";  

            $connection = new PDO("mysql:host={$host};dbname={$database};charset=utf8", $user, $password);  
            $query = $connection->query("SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_TYPE='BASE TABLE'");  
            $tables = $query->fetchAll(PDO::FETCH_COLUMN);

            if (empty($tables)) {
                echo "<p>There are no tables in database \"{$database}\".</p>";
            } else {
                echo "<p class='text-center'>Database \"{$database}\" has the following tables:</p>";
                echo "<div class='row'><div class='col-6 offset-md-3'>";

                echo "<ul class='list-group mb-4'>";
                    foreach ($tables as $table) {
                        echo "<li class='list-group-item list-group-item-action text-center'>{$table}</li>";
                    }
                echo "</ul>";

                echo "</div></div>";
            }
        ?>
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
</body>
</html>